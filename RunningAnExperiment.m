% RUNNING AN ODOR DELIVERY EXPERIMENT: 
% CODE IS BASED ON MEHRAB/YICHUN'S SESSION-BASED ACQUISITION
delete (instrfind)
function [vial_switch,olfSS,ValveState]=connectToUSB6501_GT
% This function connect MATLAB to the NIDAQ board that controls the valves.
% You won't (likely) need to run this ibecause most of the other stiulus delivery functions with call this automatically.  
% BUT it may happen - particularly if the system has been tuirned off before you come to use it.  In that case just run it.  
% If you run it and the system has been *ON* --- it may give an error saying the session has already been created.
% I think I fixed this by declaring global variables appropriately, but in case not and you get such an error:
% 1. CLOSE ScanImage
% 2. daqreset
% 3. RESTART ScanImage

function params=set_ADO_params_201901()
% This function is how you establish the parameters for the odor delivery
% It also does LED delivery and multiple odor pulses but I haven't checked whether that part of the code is working accurately
% Function reads a .txt file with a pointer to an .xls or .csv file listing the odors
% That file is E:\Data\Glenn\OdorList_GT_20190123.csv (Mehrab's version was: E:\Turner lab\Bitbucket_repos\general_code\IDnF_rig_code_20171031\Olfactometer\NewOlfactometer\calibration\odorList.xls
% This odor list is the same as what Martin was using (so all the
% olfactometer lines will not be contaminated).

function PresentOdors_201901(params, User, scale_isi) % SET scale_isi = 0 ; % 0 MEANS YOU DON'T SCALE AND ISI IS STRAIGHTFORWARD
% User is YOUR NAME - so the data is saved to the appropriate folder in 'E:\Data\User'
% Data directories must use the naming convention:  YYYYMMDD_X
% Where X = the number of the experiment on that day (1 for the first fly etc)
% This function itself calls several other functions including:
% 1) DirectoryFinder(User)
% sets the directory to the most recent one in 'E:\Data\User'
% This is where you'll save the params.mat file and the PIDData
% >>**Double check that I do save the params.mat file in this directory
% 2) FlipValve_GT
% 3) InjectOdour_GT
% 4) ShutAllValves_GT
% 5) TriggerScanImage_GT
% In these functions I set the following global variables:
% global vial_switch olfSS ValveState TriggerState ; 

% >>**MAKE SURE THAT THESE ARE IN FACT ALL SET CORRECTLY SO YOU'RE NOT ACCIDENTALLY OVERWRITING

% IMPORTANT INFORMATION:
% 1) COM PORT LIST FOR IMAGING RIG 1 (MARTINS RIG):
% Alicat controller is COM10 (was COM12 in previous code)
% PID Acquisition is AI1 on the PXI1Slot4 card(WAS Dev3 in previous code)
% ScanImage TRIGGER comes from CHANNEL6 NI6509 according to the code in triggerPV.m
% The relevant line for the trigger channel was specified as:
% flipLine6509(6,1) % SEE HELP SECTION FOR HOW LINE IS SPECIFIED BECAUSE IT'S NOT STRAIGHTFORWARD 
% I believe LINE 6 here corresponds to USB-6509 port 5 line 5
% I had to convert this code to run session-based and flip the right line.
% I took FlipValveUSB6526.m as inspiration.
% I at least confirmed that when I flip that port in the new session-based code I do see the appropriate signal on the oscilloscope

% 2) Something left over from Martin for running ScanImage I guess????
% cd 'C:\Program Files\Vidrio\SI2017bR1_2018-01-29-154311_2738ccd591\'


%% NOTES:
% 1) FlipValve_EP
%    injectOdour_EP 
% NEED TO BE CHANGED TO _GT VERSIONS IN THE OTHER FUNCTIONS THAT CALL THEM 
% XX DONE

% 2) I'M NOT SURE WHETHER THE VALVES ARE REALLY SET TO THE LOW ENERGY STATE WITH THE CURRENT CODE


%% THINGS TO DO
%  - FIGURE OUT PID ACQUISITION SO IT INCREMENTS BY TRIAL
% XX DONE
% - CHECK  HOW MUCH TIME I NEED TO ODORIZE THE SYSTEM - THIS IS HARD CODED IN PresentOdors_201901(params, User, scale_isi)
- slot 39 for shuttle valve


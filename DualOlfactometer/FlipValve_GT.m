function FlipValve_GT(lines,state)
%Flips valves through USB6509 on ImagingRig1
%
%Input
%   -lines: determines which line to flip.  You can determine with number
%   (Index number of USB6501 lines) or character of valve names (i.e. 'Vial1', 'Vial2', 'NO')
%   Or you can use 'all' to flip all valves
%   You can also define multiple valves with cell array.
%
%   -state: 0 or 1.  **Caution: 0 is energized state**
%

% DECLARE GLOBAL VARIABLES
% NEED TO DO THIS TO SHARE THE olfSS ACQUISITION SESSION ACROSS FUNCTIONS
global vial_switch olfSS ValveState TriggerState
% global vial_switch olfSS ValveState TriggerState; %see connectToUSB6509_GT.m

%ONLY CONNECT TO THE USB-6509 IF THESE GLOBALS HAVEN'T ALREADY BEEN SET UP  
if isempty(olfSS)
    [vial_switch,olfSS,ValveState,TriggerState]=connectToUSB6501_GT ; 
end

if isnumeric(lines)
    ValveState(lines)=state;    %If numeric values for lines entered set those entries in ValveState to the state specified
elseif ischar(lines) && strcmpi(lines,'all')
    ValveState(1:end)=state;    %If lines specified by 'all' set all entries in ValveState to the state specified
elseif ischar(lines)
    lines={lines};              %If lines specified by character inputs pass that as a cell array
end


if iscell(lines)
    if isscalar(state)
        state=state*ones(size(lines));  %State entry in function input becomes default for all lines
    end
    for i=1:length(lines)
%         if strcmpi(lines{i},'vial9')
%             lines{i}='NO';
%         end
        ValveState(strcmpi(lines{i},vial_switch(:,1)))=state(i);    %String Match the lines cell array
    end
end
disp(ValveState)
outputSingleScan(olfSS,[ValveState TriggerState]);
function ShutAllValves_GT

%ONLY CONNECT TO THE BOARD THE FIRST TIME THE FUNCTION IS CALLED.  
% global vial_switch olfSS ValveState TriggerState; %see connectToUSB6509_GT.m
global vial_switch olfSS ValveState TriggerState 

if isempty(olfSS)
    [vial_switch,olfSS,ValveState, TriggerState] = connectToUSB6501_GT ;
end

ValveState = ones(1,length(vial_switch)) ;
% TriggerState = 0 ;
disp(ValveState)
outputSingleScan(olfSS,[ValveState TriggerState]) ;

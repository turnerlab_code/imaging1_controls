% Running an experiment with new code

%% 0
% make E:\Documents\MATLAB\Olfactometer\DualOlfactometer the default
% directory



%% 1
% Define params
params = set_ADO_params_PABA_EL;
% modify this function so that you can deliver the odours that you want to

%% 2
% Ensure that the PMT shutters are in the normally closed state. They are
% not controlled by the arduino board located to the left of the computer
% monitor. This arduino receives inputs from scanimage about the state of
% the shutters and from the LED arduino board (located on the right of
% monitor). It combines this to decide when to open and when to close the 
% PMT shutters. If you want to test this arduino out to make  sure that it 
% is working you can check out the arduino code stim_control_shutter.ino
% and shutter_control.ino. Alternatively PXI1Slot2/ctr1 and PXI1Slot2/ctr2
% can be used through NI Max to test that the arduino is working appropriately

%% 3

% If you choose to use Yoshi's olfactometer (olf2) you will need to modify
% the delay between olf1 and olf2 to correspond with the that of the odors
% you choose to use. Once you know how much time it takes for the odour to
% reach the fly after olf2 is switched on you can edit the following mat
% file :
% E:\Documents\MATLAB\Olfactometer\DualOlfactometer\calibration\olf1_olf2_delay.mat
%

%% 4
% Once scan image is up and running and in Loop mode. You can run the
% following code to deliver odor and trigger imaging.

present_odours_modular_stim_led_AR(params, 0, 'Adithya')

%% 5 
% if you interrupt a trial midway the code will Trigger scanimage twice to
% put the Yoshi olfactometer to sleeep and disconnect it so that no errors
% occur when the next trial is run. If you run into any errors with this
% please use the following code to disconnect from COM ports

 close_serial_port(n)
 
 % n = 3  - Yoshi olfactometer on COM3
 % n = 5  - LED arduino on COM5
 
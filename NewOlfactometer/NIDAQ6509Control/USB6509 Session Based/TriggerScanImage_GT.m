function TriggerScanImage_GT(TriggerValue,verbose)
% function TriggerScanImage_GT
%
% SEND TRIGGER SIGNAL TO ScanImage
% SET UP ScanImage SO THAT: 
% TRIGGER = 1 MEANS DO SCAN
% TRIGGER = 0 MEANS STOP SCAN

if nargin==1, verbose=0; end
if verbose
    disp('Triggering ScanImage')
end

% DECLARE GLOBAL VARIABLES
% NEED TO DO THIS TO SHARE THE olfSS ACQUISITION SESSION ACROSS FUNCTIONS
global vial_switch olfSS ValveState TriggerState ; %see connectToUSB6509_GT.m

%ONLY CONNECT TO THE USB-6509 IF THESE GLOBALS HAVEN'T ALREADY BEEN SET UP  
if isempty(olfSS)
    [vial_switch,olfSS,ValveState,TriggerState] = connectToUSB6509_GT ;
end
TriggerState = TriggerValue ; 
% TriggerValue IS THE FINAL VALUE IN THE VECTOR
% PASSES THE CURRENT ValveState AS A GLOBAL
outputSingleScan(olfSS,[ValveState TriggerState]);
function ShutAllValves_EP
 
global vial_switch olfSS ValveState TriggerState; %see connectToUSB6501_GT.m

%ONLY CONNECT TO THE BOARD THE FIRST TIME THE FUNCTION IS CALLED. 
if isempty(olfSS)
    [vial_switch,olfSS,ValveState, TriggerState] = connectToUSB6501_EP ;
end

ValveState = ones(1,length(vial_switch)) ;
% TriggerState = 0 ;
disp(ValveState)
outputSingleScan(olfSS,[ValveState TriggerState]) ;

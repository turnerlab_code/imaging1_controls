function FlipValve_ep(lines,state)
%Flips valves through USB6501 on ImagingRig1
%
%Input
%   -lines: determines which line to flip.  You can determine with number
%   (Index number of USB6501 lines) or character of valve names (i.e. 'Vial1', 'Vial2', 'NO')
%   Or you can use 'all' to flip all valves
%   You can also define multiple valves with cell array.
%
%   -state: 0 or 1.  **Caution: 0 is energized state**
%

%% DECLARE GLOBAL VARIABLES
% NEED TO DO THIS TO SHARE THE olfSS ACQUISITION SESSION ACROSS FUNCTIONS
global vial_switch olfSS ValveState TriggerState
% global vial_switch olfSS ValveState TriggerState; %see connectToUSB6509_GT.m

%% ONLY CONNECT TO THE USB-6501 IF THESE GLOBALS HAVEN'T ALREADY BEEN SET UP  
if isempty(olfSS)
    [vial_switch,olfSS,ValveState,TriggerState]=connectToUSB6501_GT ; 
end
%%
if isnumeric(lines)
    ValveState(lines)=state;
elseif ischar(lines) && strcmpi(lines,'all')
    ValveState(1:end)=state;
elseif ischar(lines)
    lines={lines};
end


if iscell(lines)
    if isscalar(state)
        state=state*ones(size(lines));
    end
    for i=1:length(lines)
%         if strcmpi(lines{i},'vial9')
%             lines{i}='NO';
%         end
        ValveState(strcmpi(lines{i},vial_switch(:,1)))=state(i);
    end
end
disp(ValveState)
outputSingleScan(olfSS,[ValveState TriggerState]);
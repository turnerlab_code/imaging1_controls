UserDirectory = ['E:\Data\' User '\'] ; 
% IDENTIFY THE LAST MODIFIED DIRECTORY WITHIN THE *USER* DIRECTORY
UserDirInfo = dir(UserDirectory) ;
[~,UDidx] = max([UserDirInfo.datenum]) ;
foldername = UserDirInfo(UDidx).name;
% CAN ENSURE THAT THE FILE IS A DIRECTORY BY QUERYING isdir FIELD


DateString = datestr(now,'yyyymmdd') ; 
% regexp({UserDirInfo.name},DateString)
x = zeros(size(UserDirInfo))
for n = 1:length(UserDirInfo)
    FolderName = UserDirInfo(n).name ;
    x = regexp(FolderName,DateString)  
end

C = {UserDirInfo.name};
idx = ~cellfun('isempty',regexp(C,DateString);
[a, b] = sort([C]);

idx = ~cellfun('isempty',strfind(C, DateString));
>> C{idx}
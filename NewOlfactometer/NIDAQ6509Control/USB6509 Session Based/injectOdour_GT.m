function injectOdour_GT(vial)
if nargin<2, duration=[]; end

AIR_VIAL=13; %This is fixed unless we re-build the olfactometer

if vial==AIR_VIAL
    return
end

 FlipValve_GT({sprintf('Vial%d',vial),'NO'},0)

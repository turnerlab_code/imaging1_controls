function PresentOdors_201901(params, User, scale_isi)
% function PresentOdors_201901

% INPUT:
% params: SET WITH params=set_ADO_params_201901()
% User: STRING OF THE NAME OF THE USER e.g. 'Glenn' or 'Adithya' THAT DEFINES THE USER DIRECTORY ON 'E:\Data\'
% scale_isi:  0 LEAVES ISI TO BE WHAT YOU'D EXPECT
%             1 SCALES ISI BASED ON SOME FACTOR MEHRAB CARES ABOUT
% Written 2018 Mehrab Modi
% Edited Jan 2019 Glenn Turner
%% SETUP
% SET THE DIRECTORY FOR SAVING TO THE NEWEST DIRECTORY IN E:\Data\User
SaveDirectory = DirectoryFinder(User,1);
% CALL CLEANUP FUNCTION TO SHUT ALL VALVES IF USER PRESSES Ctrl + C
c = onCleanup(@()my_cleanup_GT());


%% IF AN EXPERIMENT IS INTERRUPTED - ALLOW USERS TO RECOVER/APPEND/OVERWRITE TO INITIAL DATASET
% DETERMINE IF THERE HAS ALEADY BEEN SOME SCANS ACQUIRED BY CHECKING IF CURRENT DIRECTORY ALREADY HAS A params FILE.
% IF YES, PROMPT USER TO SPECIFY WHETHER TO RECOVER/APPEND/OVERWRITE
if exist([SaveDirectory 'params.mat']) == 2
    button = questdlg('What would you like to do?','Old param file found!','Recover','Append','Over-write', 'Recover');
    
    %     RECOVER MEANS YOU START WITH THE SAME params
    if strcmp(button, 'Recover') == 1
        params_mat = load([SaveDirectory 'params.mat']);
        params_mat = params_mat.params_mat;
        n_trials = size(params_mat, 2);
        % IDENTIFY LAST TRIAL COMPLETED: d_tr_n
        for d_tr_n = 1:n_trials
            done_tr = params_mat(d_tr_n).trs_done;
            if done_tr == 0
                break
            else
            end
        end
        %         QUERY USER FOR WHICH TRIAL TO RE-START AT - FROM THE ORIGINAL params
        a = inputdlg('Input trial n to acquire next; 0 for last trial done.', 'Trial n', 1, {'0'});
        a = str2num(a{1, 1});
        %
        if a == 0
            start_tr = d_tr_n;      % IF ZERO ENTERED start_tr = LAST TRIAL COMPLETED
        else
            start_tr = a;           % IF A NUMBER ENTERED start_tr = THAT TRIAL NUMBER
        end
        for tr_n = start_tr:n_trials
            params_mat(tr_n).trs_done = 0;  % SET trs_done TO ZERO FOR REMAINING TRAILS
        end
        
        %         APPEND MEANS YOU GO THROUGH THE INITIAL params AND THEN ADD A SECOND SET OF TRIALS
    elseif strcmp(button, 'Append') == 1
        params_mat_old = load([SaveDirectory 'params.mat']);
        params_mat_old = params_mat_old.params_mat;
        [params_mat_new, params_spec] = setUpStimuli_trains_flex(params);
        params_mat = append_params(params_mat_old, params_mat_new);
        n_trials = size(params_mat, 2);
        % IDENTIFY LAST TRIAL COMPLETED: d_tr_n
        for d_tr_n = 1:n_trials
            done_tr = params_mat(d_tr_n).trs_done;
            if done_tr == 0
                break
            else
            end
        end
        start_tr = d_tr_n;      %start_tr PICKS UP AT THE LAST TRIAL
        % SAVE THE NEW params SPECIFICATIONS TO FILE NAMED _spec2
        save([SaveDirectory 'params_spec2.mat'], 'params_spec') ;
        
        %         OVERWRITE MEANS START AGAIN
    elseif strcmp(button, 'Over-write') == 1
        [params_mat, params_spec] = setUpStimuli_trains_flex(params);
        start_tr = 1;           %start_tr SET TO 1
    end
    
else
    % IF IT IS THE FIRST ACQUISITION INTO THIS FOLDER THEN THESE LINES ARE WHERE YOU SAVE params SPECIFICATIONS TO FILE
    [params_mat, params_spec] = setUpStimuli_trains_flex(params);
    save([SaveDirectory 'params_spec.mat'], 'params_spec') ;
    start_tr = 1 ;
end
n_trials = size(params_mat, 2);
%% INITIALIZE MFCS ETC TO PREPARE FOR ODOR DELIVERY
% OPEN SERIAL PORT TO COMMUNICATE WITH ALICAT MFCS
% FIRST CHECK IF SERIAL PORT ALREADY OPEN
AC = instrfind('Tag','Alicat_serial') ;
if isempty(AC) == 1
    AC = connectAlicat();
end

% CLOSE ALL ODOR VALVES AND SET SHUTTLE VALVE TO DIRECT CARRIER AIRSTREAM TO FLY
ShutAllValves ;

% INITIALIZE MFC FLOWS - >>NOTE THERE IS BOTH MM AND EP VERSIONS OF CODE <<
secondDilution1 = params_mat(1).secondDilution ;
firstDilution1 = params_mat(1).firstDilution ;
initialiseFlows_EP(AC, firstDilution1, secondDilution1);      %initialising flows for the first time just to set things up.
% initialiseFlows_EP(AC, 0.1, secondDilution1);      %initialising flows for the first time just to set things up.

% SET THE DURATION (sec) FOR MFC B TO INJECT INTO THE ODOR VIAL TO ODORIZE THE SYSTEM.
% NOTE: Stim_latency HAS TO BE LONGER THAN THIS >>**ADJUST CODE TO SEND WARNING**<<
od_inj_dur = 5 ;

%% SETUP ALL STIMULATION MATRICES
for trial_n = start_tr:n_trials
    
    odor_n = params_mat(trial_n).odours;
    duration = params_mat(trial_n).duration;
    firstDilution = params_mat(trial_n).firstDilution;
    secondDilution = params_mat(trial_n).secondDilution;
    n_od_pulses = params_mat(trial_n).n_od_pulses;
    inter_pulse_interval = params_mat(trial_n).inter_pulse_interval;
    stim_latency = params_mat(trial_n).stimLatency;
    od_name = params_mat(trial_n).odourNames{odor_n};
    post_od_scan_dur = params_mat(trial_n).post_od_scan_dur;
    pulse_train = params_mat(trial_n).rand_train;
    pulse_type = params_mat(trial_n).pulse_type;
    
    %listing parameters and communicating with led/elec stimulating Arduino
    %if necessary for current trial
    if params_mat(trial_n).led_on == 1 || params_mat(trial_n).elec_on == 1      % CONDITION TO CHECK IF LED OR ELEC STIM IS BEING DELIVERED ON CURRENT TRIAL
        if params_mat(trial_n).led_on == 1
            LED_elec = 0;
            
        elseif params_mat(trial_n).elec_on == 1
            LED_elec = 1;
        end
        init_delay_ms = params_mat(trial_n).stim_init_delay_ms;
        duration_ms = params_mat(trial_n).stim_dur;
        freq_hz = params_mat(trial_n).stim_freq;
        duty_cyc_percent = params_mat(trial_n).st_duty_cyc;
        
        %COMMUNICATE STIMULUS PARAMETERS TO ARDUINO
        stim_arduino_serial_comm(LED_elec, init_delay_ms, duration_ms, freq_hz, duty_cyc_percent);
    else
    end
    
    if pulse_type == 1
        tot_tr_dur = stim_latency + duration + ...
            (duration + inter_pulse_interval).*(n_od_pulses - 1) + post_od_scan_dur;
    elseif pulse_type == 0
        tot_tr_dur = stim_latency + duration + ...
            (duration + duration).*(n_od_pulses - 1) + post_od_scan_dur;
    else
    end
    
    if scale_isi == 0
        isi = params_mat(trial_n).isi;
    elseif scale_isi == 1
        isi = max([60, ((tot_tr_dur - stim_latency - post_od_scan_dur).*3)]);         %scales isi to stim duration, with a minimum isi of 60s
        isi = min([isi, 130]);                                                        %ensures scaled isi doesn't exceed 130s
        params_mat(trial_n).isi = isi;
    end
    
    % DISPLAY MESSAGES WITH TOTAL TIME UNTIL END OF ACQUISITION & OTHER INFO
    n_trials_left = n_trials - trial_n + 1;
    tot_time = round((n_trials_left.*isi)./60);  %in min
    disp([int2str(tot_time), ' minutes left for completion of acquisition.']);
    disp(['Trial ' int2str(trial_n) ' of ' int2str(n_trials) '.'])
    disp(['Delivering Odor ' int2str(odor_n) ': ' od_name '.'])
    del_conc = CalcTotalDilution(firstDilution, secondDilution).*100;
    %     disp(['Concentration delivered ' num2str(del_conc) '%.'])
    %     disp(['duration ' num2str(duration) 's, n pulses ' int2str(n_od_pulses) '.'])
    
    %     CHECK TO SEE IF ODORIZING SYSTEM HAS TO BEGIN PRIOR TO SCAN TRIGGER OR CAN HAPPEN LATER
    if stim_latency < od_inj_dur
        od_fill_early = 1;
        disp('odorizing system early')
    elseif stim_latency >= od_inj_dur
        od_fill_early = 0;
    else
    end
    
    
    %% SET UP PID ACQUISITION - WILL RUN IN THE BACKGROUND LATER
    PIDAcq = daq.createSession('ni');
    addAnalogInputChannel(PIDAcq,'PXI1Slot4', 1, 'Voltage');    %SLOT NUMBER IS ON THE NIDAQ BOX BELOW - THE NUMBER IN THE CIRCLE BELOW THE SLOT
    acq_rate = 2000 ;           % Hz
    PIDAcq.Rate = acq_rate ;
    PIDAcq.DurationInSeconds = tot_tr_dur;
    % I THINK THAT THIS LISTENS TO DATA COMING IN FROM PIDAcq AND ONCE A
    % CERTAIN DATA LENGTH IS EXCEEDED YOU EXECUTE @AcqDataBkgd TO WRITE THE DATA TO FILE
    lh = addlistener(PIDAcq,'DataAvailable', @AcqDataBkgd);     %@AcqDataBkgd FUNCTION IS AT BOTTOM OF THIS FILE
    %IF YOU WANT TO SEE THE PID DATA: lh = addlistener(PIDAcq,'DataAvailable', @plotData) ;
    % SPECIFICY HOW LONG THE INCOMING DATA NEEDS TO BE
    PIDAcq.NotifyWhenDataAvailableExceeds = acq_rate.*tot_tr_dur;
    
    %% DELIVERING ODOR SECTION
    %     SET MFC FLOWS  ****WAS THIS ALREADY SET UP ABOVE?****
    %     initialiseFlows_EP(AC, firstDilution, secondDilution);
    if od_fill_early == 0
        tic
        t_stamp = now;
        PIDAcq.startBackground();               %START PID ACQUISITION IN THE BACKGROUND
        TriggerScanImage(1)                %SEND THE TRIGGER TO ScanImage
        %         trigger_scan(1);                        %TRIGGER ScanImage TO START IMAGE ACQUISITION - **FROM MEHRAB'S ARDUINO SETUP**
        pause(stim_latency - od_inj_dur)        %PAUSE BEFORE ODORIZING THE SYSTEM
        injectOdour(odor_n)                  %ODORIZE THE SYSTEM BY SWITCHING MFC B FLOW FROM EMPTY TO ODOR VIAL
        pause(od_inj_dur)                       %PAUSE TO ALLOW SYSTEM TO ODORIZE
        
    elseif od_fill_early == 1                   %IN THE CASE WHERE ODOR ONSET IS EARLY AND YOU HAVE TO ODORIZE BEFORE TRIGGERING ScanImage
        injectOdour(odor_n)                  %ODORIZE THE SYSTEM BY SWITCHING MFC B FLOW FROM EMPTY TO ODOR VIAL
        pause((od_inj_dur - stim_latency));     %PAUSE TO ALLOW SYSTEM TO ODORIZE
        tic
        t_stamp = now;
        PIDAcq.startBackground();               %START PID ACQUISITION IN THE BACKGROUND
        TriggerScanImage(1)                %SEND THE TRIGGER TO ScanImage
        %         trigger_scan(1);                        %TRIGGER ScanImage TO START IMAGE ACQUISITION - **FROM MEHRAB'S ARDUINO SETUP**
        %         disp('pre odor scanning...')
        pause(stim_latency)                     %PAUSE UNTIL ODOR ONSET
    else
    end
    
    %     FLIP SHUTTLE VALVE TO DELIVER ODOR PULSE
    disp('odor ON')
    for r_pulse_n = 1:size(pulse_train, 1)
        pause(pulse_train(r_pulse_n, 1));
        FlipValve('Final',0)  % THE SHUTTLE VALVE RIGHT BEFORE THE FLY
        pause(pulse_train(r_pulse_n, 2));
        FlipValve('Final',1)
    end
    disp('odor OFF')
    pause(4)                            % Adding this pause makes the PID decay trace more smooth
    ShutAllValves;                   %CLOSE ALL VALVES
    %     disp('post odor scanning...')
    pause(post_od_scan_dur-4)           %PAUSE FOR THE POST-ODOR PERIOD (Correcting for 1s pause above)
    TriggerScanImage(0)              %END IMAGE ACQUISITION
    
    %LOG CURRENT TRIALS AS DONE
    params_mat(trial_n).trs_done = t_stamp;              %time stamp RECORDED AT THE BEGINNING OF THE TRIAL
    save([SaveDirectory 'params.mat'], 'params_mat');    %SAVE THE params FOR EACH TRIAL
    
    
    %     PID TRACE FILE IS SAVED IN THE BACKGROUND ON EACH TRIAL
    %     THIS LOADS THE LAST TRACE AND UPDATES THE NAME SO IT HAS A TRIAL
    %     NUMBER ON IT INSTEAD OF last_PID_trace
    load([SaveDirectory 'last_PID_trace.mat']);
    save([SaveDirectory 'PID_tr_' int2str(trial_n) '.mat'], 'PIDdata');
    
    
    %     disp('Updated param-file. Waiting for isi.');
    %     disp(' ')
    %     disp(' ')
    
    %BEFORE INITIATING ANOTHER LONG ODOR TRIAL, TRIGGER A SHORT NO-ODOR TRIAL THAT CAUSES SCANIMAGE TO WRITE THE LONG TRIAL TO DISK
    pause(1)
    TriggerScanImage(1)
    pause(1)
    TriggerScanImage(0)
    
    %PAUSE FOR THE INTER-STIMULUS-INTERVAL
    if trial_n < n_trials
        if od_fill_early == 0
            pause(isi-toc)
        elseif od_fill_early == 1
            pause(isi - toc - (od_inj_dur - stim_latency))      %accounting for the extra seconds spent odorising the olfactometer before triggering scan.
        else
        end
    else
    end
    
end
% RELEASE THE PID ACQUISITION SESSION
release(PIDAcq)
end

% A FUNCTION TO RESET THE SYSTEM
function [] = my_cleanup_GT()
TriggerScanImage(0)  %SEND STOP SIGNAL TO ScanImage
ShutAllValves;       %SHUT ALL THE VALVES
end

% A FUNCTION TO SAVE THE PID DATA ACQUIRED IN THE BACKGROUND
function PIDdata = AcqDataBkgd(src, event)
%This function is called by the listener in the background acquisition to
%acquire the PID signal and write it to file.

PIDdata = [event.Data, event.TimeStamps];
CA = DirectoryFinder('Adithya') ;
save([CA 'last_PID_trace.mat'], 'PIDdata');
% I COULD I MAKE THE DIRECTORY THE SaveDirectory FROM THE MAJOR FILE - WOULD IT STILL CALL THE SANE User?
end

% A FUNCTION TO PLOT THE PID DATA ACQUIRED IN BACKGROUND - TYPICALLY NOT USED
function plotData(src,event)
plot(event.TimeStamps, event.Data)
end
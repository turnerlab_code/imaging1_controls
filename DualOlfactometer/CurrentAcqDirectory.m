function CurrentAcqDirectory = DirectoryFinder(User)
%function CurrentAcqDirectory()

% INPUT: User: STRING OF THE NAME OF THE USER e.g. Glenn or Adithya THAT DEFINES THE USER DIRECTORY ON 'E:\Data\'
% AUTOMATICALLY IDENTIFIES THE DIRECTORY CURRENTLY BEING USED FOR ScanImage ACQUISITION
% SEARCHES IN A HARD-CODED MotherDirectory FOR THE DATE-FORMAT FOLDER WITH THE MOST RECENT (GREATEST) DATE NUMBER
% Mehrab Modi
% Edited Glenn Turner Jan 2019 FOR DIRECTORY NAMING CONVENTION: YYYYMMDD_X
% WHERE X = EXPERIMENT ON THAT DAY
%%

UserDirectory = ['E:\Data\' User '\'] ; 
% RETURN CELL ARRAY OF ALL FILE NAMES NOTE: THIS GIVES ALL FILES, NOT JUST DIRECTORIES
C = {UserDirInfo.name};
% FIND THE DIRECTORIES FROM TODAY - HAVE TO MATCH THE CURRENT DateString
DateString = datestr(now,'yyyymmdd') ; 
idx = find(~cellfun('isempty',regexp(C,DateString))) ;
% SORT TODAY'S DIRECTORIES AND TAKE THE LAST ONE AS THE CurrentDirectory
TodayDirectories = sort([C(idx)]) ; 
CurrentDirectory = TodayDirectories(end) ; 
% 
% [a, b] = sort([C]);
% 
% idx = ~cellfun('isempty',strfind(C, DateString));
% UserDirectory = ['E:\Data\' User '\'] ; 
% % IDENTIFY THE LAST MODIFIED DIRECTORY WITHIN THE *USER* DIRECTORY
% UserDirInfo = dir(UserDirectory) ;
% [~,UDidx] = max([UserDirInfo.datenum]) ;
% foldername = UserDirInfo(UDidx).name;
% CAN ENSURE THAT THE FILE IS A DIRECTORY BY QUERYING isdir FIELD
% 
% C = {UserDirInfo.name};
% 
% 
% DateString = datestr(now,'yyyymmdd') ; 
% for n = 1:3
%     FolderName = UserDirInfo(n).name ; 
% end
%     regexp(n) = (FolderName,DateString) ; 
% end

% % MAKE SURE THAT DIRECTORY HAS THE HIGHEST INDEX FOR THE DAY'S DATE
% DataDirectoryBase = [UserDirectory DateString '\']
% TF = strncmp(DateString,DataDirectoryBase,4)
% regexp(DataDirectoryBase,DateString)
% UserDirectory
% [~,UDidx] = max([UserDirInfo.datenum]);
% foldername = MDInfo(UDidx).name;
% 
% 
% MDInfo = dir(MotherDirectory);
% % IDENTIFY THE LAST MODIFIED **USER** DIRECTORY WITHIN THE MOTHER DIRECTORY
% % i.e. 'E:\Data\Glenn' OR 'E:\Data\Adithya'
% % **NOTE I THINK THIS IDENTIFIES THE LAST MODIFIED ITEM - DIRECTORY OR FILE**
% [~,MDidx] = max([MDInfo.datenum]);
% foldername = MDInfo(MDidx).name;
% % RE-DEFINE THE MotherDirectory BY ADDING THE MOST RECENTLY MODIFIED DIRECTORY BENEATH IT
% MotherDirectory = [MotherDirectory,foldername,'\'];
% % ========================================================================
% 
% % RETURN THE NAMES OF ALL THE FILES IN THE MotherDirectory
% % THIS SHOULD JUST BE A BUNCH OF DIRECTORIES OF THE FORM YYYYMMDD_X
% UserDirInfo = dir(MotherDirectory);
% % UserDirContents = {UserDirContents.name};
% % DO THE SAME THING TO IDENTIFY THE LAST MODIFIED ITEM
% [~,UDidx] = max([UserDirInfo.datenum]);
% foldername = MDInfo(UDidx).name;
% 
% 
% % WHAT DOES THIS DO?
% max_dir = 0;
% for DirCount = 1:length(DirContents)
%     CurrentDir = DirContents{1, DirCount};
%     if isdir([MotherDirectory CurrentDir]) == 0
%         continue
%     else
%     end
%     CurrentDir = str2num(CurrentDir);
%     if isempty(CurrentDir) == 1
%         continue
%     else
%     end
%     max_dir = max([max_dir, CurrentDir]);
% end
% 
% %mother directory or directory of the day's expt
% m_direc = [MotherDirectory int2str(max_dir) '\'];
% 
% 
% %identifying last modified directory within mother directory
% DirContents = dir(m_direc);
% date_time_list = {DirContents.date};
% time_stamp_vec = zeros(1, length(date_time_list));
% 
% for item_n = 1:length(date_time_list)
%     curr_t = date_time_list{1, item_n};
%     time_stamp_vec(1, item_n) = datenum(curr_t(12:20), 'HH:MM:SS'); 
% end
% 
% time_stamp_vec(1:2) = [];
% direc_tail = {DirContents.name};
% direc_tail = {direc_tail{1, 3:length(direc_tail)} };
% 
% %FIND MOST RECENT TIME STAMP
% dir_true = 0;
% dir_list = {DirContents.isdir};
% dir_list = {dir_list{1, 3:length(dir_list)} };
% 
% while dir_true == 0
%     [del maxi] = max(time_stamp_vec);
%     test = dir_list{maxi};
%     if test == 1
%         dir_true = 1;
%     else
%         time_stamp_vec(maxi) = 0;
%     end    
% end
% 
% direc_tail = direc_tail{maxi};
% direc = [m_direc direc_tail '\'];
% 
% end
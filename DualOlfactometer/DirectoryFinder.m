function CurrentAcqDirectory = DirectoryFinder(User)
%function DirectoryFinder()

% AUTOMATICALLY IDENTIFIES THE DIRECTORY CURRENTLY BEING USED FOR ScanImage ACQUISITION
% SEARCHES IN THE UserDirectory on E:\Data\ FOR THE DATE-FORMAT FOLDER WITH THE MOST RECENT (GREATEST) DATE NUMBER
% DATA DIRECTORIES MUST USE THE NAMING CONVENTION:  YYYYMMDD_X
% WHERE X = THE NUMBER OF THE EXPERIMENT ON THAT DAY (1 FOR FIRST ETC)
% INPUT: User: STRING OF THE NAME OF THE USER e.g. 'Glenn' or 'Adithya' THAT DEFINES THE USER DIRECTORY ON 'E:\Data\'

% Glenn Turner Jan 2019 
% >> MODIFY SO YOU CAN CHOOSE TO DISPLAY THE DIRECTORY ON CMD OR NOT <<
%% FIND THE DIRECTORY WITH THE LATEST NAME ACCORDING TO THE DIRECTORY NAMING CONVENTION: YYYYMMDD_X
UserDirectory = ['E:\Data\' User '\'] ; 
UserDirInfo = dir(UserDirectory) ;
% RETURN CELL ARRAY OF ALL FILE NAMES NOTE: THIS GIVES ALL FILES, NOT JUST DIRECTORIES
C = {UserDirInfo.name} ;
% FIND THE DIRECTORIES FROM TODAY - HAVE TO MATCH THE CURRENT DateString
DateString = datestr(now,'yyyymmdd') ; 
NameIdx = find(~cellfun('isempty',regexp(C,DateString))) ;
% SORT TODAY'S DIRECTORIES AND TAKE THE LAST ONE AS THE CurrentDirectory
TodayDirectories = sort([C(NameIdx)]) ; 
DirString = char(TodayDirectories(end)) ;
CurrentAcqDirectory =  ['E:\Data\' User '\' DirString '\'] ; 
disp(['Saving to ' CurrentAcqDirectory])

% %% CHECK THAT THIS DIRECTORY IS THE MOST RECENTLY MODIFIED ONE
% >>**THIS WOULD WORK IF I COULD CULL OUT THE DIRECTORIES WITH NAMES '.' AND '..'
% [~,TimeIdx] = max([UserDirInfo.datenum]) ;
% foldername = UserDirInfo(TimeIdx).name ;
% disp(foldername)
% % mother_direc = [mother_direc,foldername,'\'];


%% SETUP

function[count] =  Acq_PID_AR(User1, count, odorID, pulse_time)
% SET THE DIRECTORY FOR SAVING TO THE NEWEST DIRECTORY IN E:\Data\User
global User
User = User1;
SaveDirectory = DirectoryFinder(User);
% % CALL CLEANUP FUNCTION TO SHUT ALL VALVES IF USER PRESSES Ctrl + C
% c = onCleanup(@()my_cleanup_GT());

%%

PIDAcq = daq.createSession('ni');
addAnalogInputChannel(PIDAcq,'PXI1Slot4', 1, 'Voltage');    %SLOT NUMBER IS ON THE NIDAQ BOX BELOW - THE NUMBER IN THE CIRCLE BELOW THE SLOT
acq_rate = 2000 ;           % Hz
PIDAcq.Rate = acq_rate;
odor_vec = odorID * ones(5,1); % seconds
pulse_train = zeros(5,2) + pulse_time; % seconds
init_wait_time = 1; % seconds
tot_tr_dur = sum(sum(pulse_train)) +  init_wait_time; % seconds

PIDAcq.DurationInSeconds = tot_tr_dur;
lh = addlistener(PIDAcq,'DataAvailable', @AcqDataBkgd);
PIDAcq.NotifyWhenDataAvailableExceeds = acq_rate.*tot_tr_dur;

if count == 0
    olf_arduino_serial_comm(0, pulse_train, odor_vec, init_wait_time)
else
    olf_arduino_serial_comm(1, pulse_train, odor_vec, init_wait_time)
end

Data = PIDAcq.startForeground();

load([SaveDirectory 'last_PID_trace.mat']);
count = count + 1;
save([SaveDirectory 'PID_tr_' int2str(count) '.mat'], 'PIDdata');



% A FUNCTION TO SAVE THE PID DATA ACQUIRED IN THE BACKGROUND
function PIDdata = AcqDataBkgd(src, event)
%This function is called by the listener in the background acquisition to
%acquire the PID signal and write it to file.
PIDdata = [event.Data, event.TimeStamps];
CA = DirectoryFinder(User) ;
save([CA 'last_PID_trace.mat'], 'PIDdata');
% I COULD I MAKE THE DIRECTORY THE SaveDirectory FROM THE MAJOR FILE - WOULD IT STILL CALL THE SAME User?
end


end

function [] = prime_olf2(od_list)

if isempty(od_list) == 1
    od_list = 1:4;
else
end

n_pulses = length(od_list).*10;

pulse_train = zeros(n_pulses, 2) + 5;
%pulse_train(:, 2) = 15;
od_vec = repmat(od_list, 1, 10);
od_vec = reshape(od_vec, 1, []);

olf_arduino_serial_comm(0, pulse_train, od_vec, 1);
pause(0.5)

TriggerScanImage_GT(1,1);
pause(1)
TriggerScanImage_GT(0);
pause(size(pulse_train, 1).*10);

sleep_olf2;
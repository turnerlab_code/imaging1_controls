function ShutAllValves(verbose)

%ONLY CONNECT TO THE BOARD THE FIRST TIME THE FUNCTION IS CALLED.  
% global vial_switch olfSS ValveState TriggerState; %see connectToUSB6509_GT.m
global vial_switch olfSS ValveState TriggerState 

if isempty(olfSS)
    [vial_switch,olfSS,ValveState, TriggerState] = connectToUSB6501_GT ;
end

ValveState = ones(1,length(vial_switch)) ;
outputSingleScan(olfSS,[ValveState TriggerState]) ;     %KEEP CURRENT STATE OF ScanImage TRIGGER

if nargin==0, verbose=0; end
if verbose
    disp(['Shutting ALL Valves.  ValveState: ' num2str(ValveState)])
%     disp(['Valve State is: ' num2str(ValveState)])
end